<?php
get_header();
?>

<div class="service__item--background">
    <main class="eastar_main">
        <div class="main__container">
            <?php
            while (have_posts()) {
                the_post();

                $title = get_the_title();
                $categories = get_the_category();
                $category_title = get_theme_mod('event_breadcrumb');

                set_query_var('breadcrumb_title', $title);
                set_query_var('category_title', $category_title);

                get_template_part('includes/breadcrumb');
            }
            ?>

        </div>

        <div class="news__single">
            <div class="main__container">
                <div class="container">
                    <?php while (have_posts()) {
                        the_post(); ?>
                        <div class="row">
                            <div class="col-12 text-center">
                                <h2>
                                    <?php echo the_title(); ?>
                                </h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <?php echo the_content(); ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
</div>
</main>

<?php
get_footer();
?>