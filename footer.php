<footer class="eastar__footer">
    <div class="eastar__footer--top">
        <div class="container">
            <div class="row ">
                <div class="col-12 col-sm-12 col-xl-6">
                    <div class="d-flex h-100 align-items-center">
                        <div class="eastar__footer--logo ">
                            <?php if (function_exists('the_custom_logo')) {
                                echo get_custom_logo();
                            } ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-3 mt-3">
                    <div class="eastar__footer--center">
                        <div class="item">
                            <div class="item--icon">
                                <i class="fa-solid fa-building"></i>
                            </div>
                            <div class="item--content">
                                Văn phòng: <span>
                                    <?php echo get_theme_mod('address_office') ?>
                                </span>
                                <a href="<?php echo get_theme_mod('address_office_link') ?>" class="item--detail" target="_blank">Xem chi tiết</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item--icon">
                                <i class="fa-solid fa-phone-volume"></i>
                            </div>
                            <div class="item--content">
                                Điện thoại: <a href="<?php echo 'tel:' . get_theme_mod('phone') ?>"><?php echo get_theme_mod('phone') ?></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item--icon">
                                <i class="fa-solid fa-headphones"></i>
                            </div>
                            <div class="item--content">
                                Đường dây nóng: <a href="<?php echo 'tel:' . get_theme_mod('hotline') ?>"><?php echo get_theme_mod('hotline') ?></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item--icon">
                                <i class="fa-solid fa-envelope"></i>
                            </div>
                            <div class="item--content">
                                E-mail:
                                <a href="<?php echo 'mailto:' . get_theme_mod('email') ?>" class="text-decoration-none"><?php echo get_theme_mod('email') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-xl-3 mt-3">
                    <div class="eastar__footer--right">
                        <div class="item">
                            <div class="item--icon">
                                <i class="fa-solid fa-house-chimney"></i>
                            </div>
                            <div class="item--content">
                                Chỗ ở học viên: <span>
                                    <?php echo get_theme_mod('student_address') ?>
                                </span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="item--icon">
                                <i class="fa-solid fa-location-dot"></i>
                            </div>
                            <div class="item--content">
                                Địa chỉ đăng ký kinh doanh: <span>
                                    <?php echo get_theme_mod('business_address') ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="eastar__footer--bot">
        <a href="#">
            <?php echo get_theme_mod('copyrights'); ?>
        </a>
    </div>
</footer>

<?php
wp_footer();
?>
</body>

</html>