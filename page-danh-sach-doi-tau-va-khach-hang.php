<?php
get_header();
?>

<main class="eastar_main">
    <div class="main__container">
        <?php
        $title = get_theme_mod('fleet_breadcrumb');
        $category_title = '';
        $category_url = '';

        set_query_var('breadcrumb_title', $title);
        set_query_var('category_title', $category_title);
        set_query_var('category_url', $category_url);

        get_template_part('includes/breadcrumb'); ?>
    </div>

    <div class="table__fleet">
        <div class="main__container">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center pt-26">
                        <h2>
                            <?php echo get_theme_mod('table_fleet_title'); ?>
                        </h2>
                    </div>

                    <div class="col-12 text-center">
                        <h3>
                            <?php echo get_theme_mod('table_fleet_des'); ?>
                        </h3>
                    </div>

                    <div class="col-12 table__fleet--table pe-0">
                        <?php echo do_shortcode('[table id=1 /]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('includes/front', 'advance-client'); ?>

</main>

<?php
get_footer();
?>