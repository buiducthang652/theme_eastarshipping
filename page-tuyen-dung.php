<?php
get_header();
?>

<div class="service__item--background">
    <main class="eastar_main">
        <div class="main__container">
            <?php
            $title = 'Tuyển dụng';
            $category_title = '';
            $category_url = '';

            set_query_var('breadcrumb_title', $title);
            set_query_var('category_title', $category_title);
            set_query_var('category_url', $category_url);

            get_template_part('includes/breadcrumb'); ?>

        </div>

        <div class="service__title">
            <div class="main__container">
                <div class="container">
                    <div class="row">
                        <div class="col-12 service__title--content">
                            Bảo trì
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</main>

<?php
get_footer();
?>