<?php
get_header();
?>

<div class="service__item--background">
   <main class="eastar_main">
      <div class="main__container">
         <?php
         $title = get_theme_mod('event_breadcrumb');
         $category_title = '';
         $category_url = '';

         set_query_var('breadcrumb_title', $title);
         set_query_var('category_title', $category_title);
         set_query_var('category_url', $category_url);

         get_template_part('includes/breadcrumb'); ?>

      </div>

      <div class="news">
         <div class="main__container">
            <div class="container">
               <div class="row">
                  <div class="col-12 text-center">
                     <h2>
                        <?php echo get_theme_mod('news_title') ?>
                     </h2>
                  </div>
               </div>
               <div class="row">
                  <?php
                  $paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
                  $args = array(
                     'post_type' => 'post', // Adjust this if you have a custom post type
                     'posts_per_page' => 3, // Set how many posts you want per page
                     'paged' => $paged,
                  );
                  $the_query = new WP_Query($args);

                  if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                  ?>
                        <div class="col-12 col-sm-4">
                           <div class="new__page--item">
                              <a href="<?php echo the_permalink(); ?>">
                                 <div class="new__page--img">
                                    <?php the_post_thumbnail(); ?>
                                 </div>
                                 <div class="new__page--detail">
                                    <h3><?php echo get_the_title(); ?></h3>
                                    <p class="detail--content">
                                       <?php echo get_the_excerpt(); ?>
                                    </p>
                                    <p>
                                       <span><i class="fa-regular fa-clock"></i></span>
                                       Ngày phát hành:
                                       <span class="new__page--clock"><?php echo get_the_date('d-m-Y'); ?></span>
                                    </p>
                                 </div>
                              </a>
                           </div>
                        </div>
                  <?php
                     endwhile;
                  endif;
                  ?>
               </div>
               <div class="row">
                  <div class="col-12">
                     <div class="news__paginate">
                        <?php
                        echo paginate_links(array(
                           'total' => $the_query->max_num_pages,
                           'current' => $paged,
                           'type' => 'list',
                           'prev_next' => true,
                           'prev_text' => __('<i class="fa-solid fa-chevron-left"></i>'),
                           'next_text' => __('<i class="fa-solid fa-chevron-right"></i>'),
                        ));
                        ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <?php get_template_part('includes/blog', 'event'); ?>
</div>
</main>

<?php
get_footer();
?>