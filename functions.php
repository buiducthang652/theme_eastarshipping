<?php
require_once get_template_directory() . '/includes/function/load-css.php';
require_once get_template_directory() . '/includes/function/load-js.php';

// Setup theme
function mic_config()
{
    register_nav_menus(
        [
            'header-menu' => 'Header Menu',
        ]
    );

    // Custom logo
    add_theme_support(
        'custom-logo',
        array(
            'height' => 30,
            'width' => 160,
            'flex-height' => true,
            'flex-width' => true,
        )
    );

    add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'mic_config', 0);

function add_aos_library()
{
    wp_enqueue_style('aos-style', 'https://cdn.jsdelivr.net/npm/aos@2.3.4/dist/aos.css');
    wp_enqueue_script('aos-script', 'https://cdn.jsdelivr.net/npm/aos@2.3.4/dist/aos.js', array(), false, true);
}
add_action('wp_enqueue_scripts', 'add_aos_library');

function add_aos_to_elements()
{
    echo '<script>
        jQuery(document).ready(function($) {
            AOS.init();
        });
    </script>';
}
add_action('wp_footer', 'add_aos_to_elements', 20);




// Header
require_once get_template_directory() . '/includes/function/customize-header.php';
require_once get_template_directory() . '/includes/function/customize-slideshow.php';
require_once get_template_directory() . '/includes/function/customize-about.php';
require_once get_template_directory() . '/includes/function/customize-vision-misson.php';
require_once get_template_directory() . '/includes/function/customize-certificate-our-team.php';
require_once get_template_directory() . '/includes/function/customize-our-activities.php';
require_once get_template_directory() . '/includes/function/customize-advance-client.php';
require_once get_template_directory() . '/includes/function/customize-dich-vu.php';
require_once get_template_directory() . '/includes/function/customize-danh-sach-doi-tau.php';
require_once get_template_directory() . '/includes/function/customize-lien-he.php';
require_once get_template_directory() . '/includes/function/customize-trung-tam.php';
require_once get_template_directory() . '/includes/function/customize-event.php';
require_once get_template_directory() . '/includes/function/customize-footer.php';
