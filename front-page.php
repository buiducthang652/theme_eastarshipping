<?php
get_header();
?>

<main class="eastar_main">
    <?php get_template_part('includes/front', 'slider'); ?>
    <?php get_template_part('includes/front', 'about'); ?>
    <?php get_template_part('includes/front', 'vision-misson'); ?>

    <div class="certificate_our_team">
        <?php get_template_part('includes/front', 'certificate'); ?>
        <?php get_template_part('includes/front', 'team'); ?>
        <?php get_template_part('includes/front', 'infomation'); ?>
    </div>

    <?php get_template_part('includes/front', 'our-activities'); ?>
    <?php get_template_part('includes/front', 'advance-client'); ?>
</main>

<?php
get_footer();
?>