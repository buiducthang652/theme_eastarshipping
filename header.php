<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
    <title>
        <?php if (is_home()) {
            echo "East Star Shipping";
        } else {
            echo get_the_title();
        } ?>
    </title>

</head>

<body <?php body_class(); ?>>
    <header class="eastar__header">
        <div class="eastar__header--sticky">
            <div class="logo">
                <?php if (function_exists('the_custom_logo')) {
                    echo get_custom_logo();
                } ?>
            </div>
        </div>
        <div class="eastar__header--icon-Menu">
            <span><i class="fa-solid fa-bars"></i></span>
        </div>
        <div class="container-fluid">
            <ul class="eastar__header--nav">
                <?php
                $menu_locations = get_nav_menu_locations();
                $menu_object = wp_get_nav_menu_object($menu_locations['header-menu']);

                if ($menu_object) {
                    $current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    $current_path = parse_url($current_url, PHP_URL_PATH);

                    $home_url = get_home_url();
                    $home_path = parse_url($home_url, PHP_URL_PATH);

                    $menu_id = $menu_object->term_id;
                    $menu_items = wp_get_nav_menu_items($menu_id);

                    if ($menu_items) {
                        foreach ($menu_items as $menu_item) {
                            $menu_item_path = parse_url($menu_item->url, PHP_URL_PATH);

                            if ($menu_item_path == $home_path) {
                                $is_active = ($current_path == $home_path || $current_path == '/') ? 'active' : '';
                            } else {
                                $is_active = ($current_path == $menu_item_path) ? 'active' : '';
                            }

                            echo '<li class="' . $is_active . '"><a href="' . $menu_item->url . '">' . $menu_item->title . '</a></li>';
                        }
                    } else {
                        echo 'Chưa có menu';
                    }
                }
                ?>
            </ul>


            <div class="eastar__header--media">
                <a href="<?php echo get_theme_mod('linkedin_link', '#') ?>">
                    <div class="media--linkedin">
                        <i class="fa-brands fa-linkedin"></i>
                    </div>
                </a>
                <a href="<?php echo get_theme_mod('facebook_link', '#') ?>">
                    <div class="media--facebook">
                        <i class="fa-brands fa-facebook"></i>
                    </div>
                </a>
                <a href="<?php echo get_theme_mod('youtube_link', '#') ?>">
                    <div class="media--youtube">
                        <i class="fa-brands fa-youtube"></i>
                    </div>
                </a>
                <a href="<?php echo get_theme_mod('twitter_link', '#') ?>">
                    <div class="media--twitter">
                        <i class="fa-brands fa-twitter"></i>
                    </div>
                </a>
            </div>
            <div class="eastar__header--lang">
                <div class="lang--logo">
                    <img src="<?php echo get_template_directory_uri() . '/assets/img/flag_vn.svg' ?>" alt="">
                </div>
                <select name="" class="lang--select" id="">
                    <option value="">Vie</option>
                    <option value="">Eng</option>
                </select>
            </div>
        </div>
    </header>
    <div class="header__ball"></div>