<?php
get_header();
?>

<div class="service__item--background">
    <main class="eastar_main">
        <div class="main__container">
            <?php
            $title = get_theme_mod('service_breadcrumb');
            $category_title = '';
            $category_url = '';

            set_query_var('breadcrumb_title', $title);
            set_query_var('category_title', $category_title);
            set_query_var('category_url', $category_url);

            get_template_part('includes/breadcrumb'); ?>

        </div>

        <div class="service__title">
            <div class="main__container">
                <div class="container">
                    <div class="row">
                        <div class="col-12 service__title--content">
                            <?php echo get_theme_mod('service_title') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="main__container">
            <div class="container">
                <div class="row ">
                    <div class="service__item">
                        <div class="col-12 col-sm-7">
                            <div class="service__item__content">
                                <div class="service__item--rank">01</div>
                                <div class="me-0 me-sm-4 text-justify">
                                    <div class="service__item__content--title">
                                        <?php echo get_theme_mod('service_title_1'); ?>
                                    </div>
                                    <div class="service__item__content--dis">
                                        <?php echo get_theme_mod('service_content_1'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5">
                            <img class="my-1" src="<?php echo get_theme_mod('service_img_1'); ?>" alt="">
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="service__item flex-rev">
                        <div class="col-12 col-sm-5">
                            <div class="">
                                <img class="my-1" src="<?php echo get_theme_mod('service_img_21'); ?>" alt="">
                            </div>
                            <div class="">
                                <img class="my-1" src="<?php echo get_theme_mod('service_img_22'); ?>" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-sm-7 ps-0 ps-sm-4">
                            <div class="service__item__content">
                                <div class="service__item--rank">02</div>
                                <div class="text-justify">
                                    <div class="service__item__content--title">
                                        <?php echo get_theme_mod('service_title_2'); ?>
                                    </div>
                                    <div class="service__item__content--dis">
                                        <?php echo get_theme_mod('service_content_2'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="service__item">
                        <div class="col-12 col-sm-7">
                            <div class="service__item__content">
                                <div class="service__item--rank">03</div>
                                <div class="me-0 me-sm-4 text-justify">
                                    <div class="service__item__content--title">
                                        <?php echo get_theme_mod('service_title_3'); ?>
                                    </div>
                                    <div class="service__item__content--dis">
                                        <?php echo get_theme_mod('service_content_3'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-5">
                            <div class="">
                                <img class="my-1" src="<?php echo get_theme_mod('service_img_31'); ?>" alt="">
                            </div>
                            <div class="">
                                <img class="my-1" src="<?php echo get_theme_mod('service_img_32'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</main>

<?php
get_footer();
?>