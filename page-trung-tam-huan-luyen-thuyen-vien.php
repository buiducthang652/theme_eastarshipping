<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css">

    <style>
        .swiper {
            padding: 0 34px;
            width: 100%;
            height: 126%;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .swiper-wrapper {
            height: 156px;
        }

        .swiper-slide a {
            width: 100%;
            height: 100%;
        }

        .swiper-slide img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .block__slider .swiper-button-next,
        .block__slider .swiper-rtl .swiper-button-prev {
            right: var(--swiper-navigation-sides-offset, 0px);
            left: auto;
            top: 80px;
        }

        .block__slider .swiper-button-next::after,
        .block__slider .swiper-rtl .swiper-button-prev::after,
        .block__slider .swiper-button-prev::after,
        .block__slider .swiper-rtl .swiper-button-next::after {
            min-width: 24px;
            min-height: 24px;
            width: 24px;
            height: 24px;
            font-size: .9rem;
            line-height: 24px;
            text-align: center;
            box-sizing: border-box;
            transition: all 0.3s ease 0s;
            background-color: rgba(103, 58, 183, 0.1);
            color: rgb(51, 51, 51);
            box-shadow: rgb(51, 51, 51) 0px 0px 2px 0px;
            border-radius: 50%;
            border: none;
            padding: 0px;
            align-self: center;
            cursor: pointer;
            outline: none;
        }

        .block__slider .swiper-button-prev,
        .block__slider .swiper-rtl .swiper-button-next {
            left: var(--swiper-navigation-sides-offset, 0px);
            right: auto;
            top: 80px;
        }

        .training__block--title {
            font-family: "UTMCafeta";
            font-style: normal;
            font-weight: 400;
            font-size: 30px;
            line-height: 38px;
            text-align: justify;
            margin-bottom: 10px;
            color: var(--text-blue);
            padding-top: 70px;
        }

        .swiper-pagination-bullet {
            box-sizing: border-box;
            padding: 0px;
            transition: all 250ms ease 0s;
            border: none;
            margin: 5px;
            background-color: rgba(103, 58, 183, 0.5);
            font-size: 1.3em;
            content: "";
            height: 10px;
            width: 10px;
            box-shadow: rgb(103, 58, 183) 0px 0px 1px 3px;
            border-radius: 50%;
            outline: none;
            background-color: initial;
        }


        .swiper-pagination-bullet-active {
            box-shadow: 0 0 1px 3px var(--bg-blue);
        }

        .lb-outerContainer {
            width: auto;
            height: auto;
            margin: 0 280px;
        }

        .lb-image {
            width: 100% !important;
            height: auto !important;
        }

        .swiper-button-lock {
            display: flex !important;
        }
    </style>
</head>

<?php
get_header();
?>

<div class="service__item--background">
    <main class="eastar_main">

        <div class="training__header">
            <div class="">
                <?php echo get_theme_mod('training_title'); ?>
            </div>
        </div>

        <div class="main__container">
            <?php
            $title = get_theme_mod('training_breadcrumb');
            $category_title = '';
            $category_url = '';

            set_query_var('breadcrumb_title', $title);
            set_query_var('category_title', $category_title);
            set_query_var('category_url', $category_url);

            get_template_part('includes/breadcrumb'); ?>
        </div>

        <div class="training__title">
            <div class="main__container">
                <?php
                $training_para = get_theme_mod('training_paragraph_count');

                for ($i = 1; $i <= $training_para; $i++) {
                ?>
                    <p>
                        <?php echo get_theme_mod('training_paragraph_' . $i); ?>
                    </p>
                <?php
                }
                ?>

            </div>
        </div>

        <div class="training__block">
            <div class="main__container">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="training__block--title">
                                <?php echo get_theme_mod('training_title_1'); ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="training__block--content">
                                <div class="swiper block__slider">
                                    <div class="swiper-wrapper">
                                        <?php
                                        $training_swiper1 = get_theme_mod('training_swiper1_img');

                                        for ($i = 1; $i <= $training_swiper1; $i++) {
                                        ?>
                                            <div class="swiper-slide">
                                                <a href="<?php echo get_theme_mod('training_swiper1_img_' . $i); ?>" data-lightbox="roadtrip" data-title="<?php echo "Hình ảnh " . $i; ?>">
                                                    <img src="<?php echo get_theme_mod('training_swiper1_img_' . $i); ?>" alt="<?php echo "Hình ảnh " . $i; ?>">
                                                </a>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="training__block--title">
                                <?php echo get_theme_mod('training_title_2'); ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="training__block--content">
                                <div class="swiper block__slider">
                                    <div class="swiper-wrapper">
                                        <?php
                                        $training_swiper2 = get_theme_mod('training_swiper2_img');

                                        for ($i = 1; $i <= $training_swiper2; $i++) {
                                        ?>
                                            <div class="swiper-slide">
                                                <a href="<?php echo get_theme_mod('training_swiper2_img_' . $i); ?>" data-lightbox="roadtrip" data-title="<?php echo "Hình ảnh " . $i; ?>">
                                                    <img src="<?php echo get_theme_mod('training_swiper2_img_' . $i); ?>" alt="<?php echo "Hình ảnh " . $i; ?>">
                                                </a>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="training__block--title">
                                <?php echo get_theme_mod('training_title_3'); ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="training__block--content">
                                <div class="swiper block__slider">
                                    <div class="swiper-wrapper">
                                        <?php
                                        $training_swiper3 = get_theme_mod('training_swiper3_img');

                                        for ($i = 1; $i <= $training_swiper3; $i++) {
                                        ?>
                                            <div class="swiper-slide">
                                                <a href="<?php echo get_theme_mod('training_swiper3_img_' . $i); ?>" data-lightbox="roadtrip" data-title="<?php echo "Hình ảnh " . $i; ?>">
                                                    <img src="<?php echo get_theme_mod('training_swiper3_img_' . $i); ?>" alt="<?php echo "Hình ảnh " . $i; ?>">
                                                </a>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                    <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

<?php
get_footer();
?>

<!-- Swiper JS -->
<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>

<!-- Initialize Swiper -->
<script>
    function updateSwiper() {
        if (window.innerWidth < 600) {
            if (swiper && swiper.params.slidesPerView !== 1) {
                swiper.params.slidesPerView = 1;
                swiper.params.slidesPerGroup = 1;
                swiper.update();
            }
        } else if (window.innerWidth < 996) {
            if (swiper && swiper.params.slidesPerView !== 2) {
                swiper.params.slidesPerView = 2;
                swiper.params.slidesPerGroup = 2;
                swiper.update();
            }
        } else {
            if (swiper && swiper.params.slidesPerView !== 3) {
                swiper.params.slidesPerView = 3;
                swiper.params.slidesPerGroup = 3;
                swiper.update();
            }
        }
    }

    var swiper = new Swiper('.block__slider', {
        slidesPerView: 3,
        slidesPerGroup: 3, // Nhóm mỗi 3 slide lại với nhau
        spaceBetween: 35,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    updateSwiper();

    window.addEventListener("resize", updateSwiper);
</script>