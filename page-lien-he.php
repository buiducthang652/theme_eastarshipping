<?php
get_header();
?>

<main class="eastar_main">
    <div class="main__container">
        <?php
        $title = get_theme_mod('contact_breadcrumb');
        $category_title = '';
        $category_url = '';

        set_query_var('breadcrumb_title', $title);
        set_query_var('category_title', $category_title);
        set_query_var('category_url', $category_url);

        get_template_part('includes/breadcrumb'); ?>

    </div>

    <div class="contact">
        <div class="main__container">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-5">
                        <div class="contact__detail">
                            <p>
                                <?php echo get_theme_mod('contact_address') ?>
                            </p>
                        </div>
                        <div class="contact__detail">
                            <p><?php echo get_theme_mod('contact_phone') ?>

                            </p>
                            <p class="text-red">
                                <?php echo get_theme_mod('contact_email'); ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 contact__detail">
                        <?php echo do_shortcode('[contact-form-7 id="f0af349" title="contact"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
get_footer();
?>