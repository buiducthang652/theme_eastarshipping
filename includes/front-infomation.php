<div class="main__container">
    <div class="container">
        <div class="main__infomation">
            <div class="row">
                <div class="col-12 col-sm-3 item__infomation">
                    <div class="infomation__title">
                        <span class="info_count">
                            <?php echo get_theme_mod('infomation_title_1'); ?>
                        </span>
                        <?php echo get_theme_mod('infomation_sign_1'); ?>
                    </div>
                    <div class="infomation__description">
                        <?php echo get_theme_mod('infomation_name_1'); ?>
                    </div>
                </div>
                <div class="col-12 col-sm-3 item__infomation">
                    <div class="infomation__title">
                        <span class="info_count">
                            <?php echo get_theme_mod('infomation_title_2'); ?>
                        </span>
                        <?php echo get_theme_mod('infomation_sign_2'); ?>
                    </div>
                    <div class="infomation__description">
                        <?php echo get_theme_mod('infomation_name_2'); ?>
                    </div>
                </div>
                <div class="col-12 col-sm-3 item__infomation">
                    <div class="infomation__title">
                        <span class="info_count"><?php echo get_theme_mod('infomation_title_3'); ?></span>
                        <?php echo get_theme_mod('infomation_sign_3'); ?>
                    </div>
                    <div class="infomation__description">
                        <?php echo get_theme_mod('infomation_name_3'); ?>
                    </div>
                </div>
                <div class="col-12 col-sm-3 item__infomation">
                    <div class="infomation__title">
                        <span class="info_count"><?php echo get_theme_mod('infomation_title_4'); ?></span>
                        <?php echo get_theme_mod('infomation_sign_4'); ?>
                    </div>
                    <div class="infomation__description">
                        <?php echo get_theme_mod('infomation_name_4'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    const elements = document.querySelectorAll(".info_count");
    const totalTime = 4000;

    function animateNumber(element, finalNumber) {
        let startTime = null;

        function updateNumber(currentTime) {
            if (!startTime) startTime = currentTime;
            const elapsedTime = currentTime - startTime;
            const progress = Math.min(elapsedTime / totalTime, 1);

            const currentNumber = Math.floor(progress * finalNumber);
            element.innerText = currentNumber.toLocaleString();

            if (elapsedTime < totalTime) {
                requestAnimationFrame(updateNumber);
            } else {
                element.innerText = finalNumber.toLocaleString();
            }
        }

        requestAnimationFrame(updateNumber);
    }

    const observer = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                const element = entry.target;
                const finalNumber = parseInt(element.textContent.replace(/\D/g, ''), 10);
                animateNumber(element, finalNumber);
            }
        });
    }, {
        threshold: 0.5
    });

    elements.forEach(el => observer.observe(el));
</script>