<?php $breadcrumb_title = get_query_var('breadcrumb_title', 'Đường dẫn trang web'); ?>
<?php $category_title = get_query_var('category_title', ''); ?>
<?php $category_url = get_query_var('category_url', ''); ?>


<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul>
                    <li>
                        <span><i class="fa-solid fa-house"></i></span>
                        <a href="<?php echo home_url(); ?>">Trang chủ</a>
                        <span class="arrow"><i class="fa-solid fa-chevron-right"></i></span>
                    </li>

                    <?php
                    if ($category_title === '' && $category_url === '') {
                        echo '';
                    } else {
                    ?>
                        <li>
                            <strong>
                                <?php echo $category_title ?>
                            </strong>
                            <span><i class="fa-solid fa-chevron-right"></i></span>
                        </li>
                    <?php
                    }
                    ?>
                    <li>
                        <strong>
                            <?php echo $breadcrumb_title ?>
                        </strong>
                    </li>
                </ul>
                <hr class="border_hr">
            </div>
        </div>
    </div>
</div>