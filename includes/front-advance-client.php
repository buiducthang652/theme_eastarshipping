<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />

    <style>
        .advance__client h2 {
            font-family: "UTMCafeta";
            text-transform: uppercase;
            font-style: normal;
            font-weight: 400;
            font-size: 36px;
            line-height: 44px;
            text-align: center;
            color: var(--text-blue);
            margin: 24px 0;
            padding-bottom: 24px;
            position: relative;
            display: inline-block;
        }

        .advance__client h2::after {
            content: "";
            position: absolute;
            width: 50%;
            bottom: 0;
            left: 25%;
            height: 5px;
            background-color: var(--orangy-yellow);
        }

        .advance__client--Swiper {
            width: 1313px !important;
            padding: 1px 64px 22px !important;
        }

        .advance__client--Swiper .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 8px;
        }

        .advance__client--Swiper .swiper-slide img {
            display: block;
            width: 100%;
            object-fit: cover;
            padding: 0;
            display: inline;
        }

        .advance__client--Swiper .swiper {
            width: 100%;
        }

        .advance__client--Swiper .swiper-wrapper {
            align-items: center;
        }

        .advance__client--Swiper .append-buttons {
            text-align: center;
            margin-top: 20px;
        }

        .advance__client--Swiper .append-buttons button {
            display: inline-block;
            cursor: pointer;
            border: 1px solid #ffffff;
            color: #ffffff;
            text-decoration: none;
            padding: 4px 10px;
            border-radius: 4px;
            margin: 0 10px;
            font-size: 13px;
        }

        @media only screen and (max-width: 1313px) {
            .advance__client--Swiper {
                max-width: 95% !important;
            }
        }
    </style>
</head>

<div class="advance__client">
    <div class="main__container">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2><?php echo get_theme_mod('advance_client_title'); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div #swiperRef="" class="swiper advance__client--Swiper">
        <div class="swiper-wrapper">
            <?php
            $advance_client = get_theme_mod('advance_client_count', 7);

            for ($i = 1; $i <= $advance_client; $i++) {
                $advanced = get_theme_mod('advance_client_img_' . $i, '');
                if (!empty($advanced)) {
            ?>
                    <div class="swiper-slide">
                        <a href="<?php echo get_theme_mod('advance_client_url_' . $i); ?>">
                            <img src="<?php echo get_theme_mod('advance_client_img_' . $i); ?>" alt="Advance Client">
                        </a>
                    </div>
            <?php
                }
            }
            ?>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

<script>
    var swiper;

    function updateSwiper() {
        if (window.innerWidth < 500) {
            if (swiper && swiper.params.slidesPerView !== 2) {
                swiper.params.slidesPerView = 2;
                swiper.update();
            }
        } else if (window.innerWidth < 900) {
            if (swiper && swiper.params.slidesPerView !== 3) {
                swiper.params.slidesPerView = 3;
                swiper.update();
            }
        } else {
            if (swiper && swiper.params.slidesPerView !== 4) {
                swiper.params.slidesPerView = 4;
                swiper.update();
            }
        }
    }

    swiper = new Swiper(".advance__client--Swiper", {
        slidesPerView: 4,
        centeredSlides: false,
        spaceBetween: 65,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        }
    });

    updateSwiper();

    window.addEventListener("resize", updateSwiper);

    var appendNumber = 4;
    var prependNumber = 1;
    document
        .querySelector(".prepend-2-slides")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.prependSlide([
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>",
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>",
            ]);
        });
    document
        .querySelector(".prepend-slide")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.prependSlide(
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>"
            );
        });
    document
        .querySelector(".append-slide")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.appendSlide(
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>"
            );
        });
    document
        .querySelector(".append-2-slides")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.appendSlide([
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>",
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>",
            ]);
        });
</script>