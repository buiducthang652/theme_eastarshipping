<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />

    <style>
        .main__our--activities-Swiper {
            width: 1313px !important;
            padding: 1px 64px 22px !important;
        }

        .main__our--activities-Swiper .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 8px;
        }

        .main__our--activities-Swiper .swiper-slide img {
            display: block;
            width: 100%;
            object-fit: cover;
            height: 275px;
            padding: 0;
            display: inline;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
        }

        .main__our--activities-Swiper .swiper {
            width: 100%;
        }

        .main__our--activities-Swiper .append-buttons {
            text-align: center;
            margin-top: 20px;
        }

        .main__our--activities-Swiper .append-buttons button {
            display: inline-block;
            cursor: pointer;
            border: 1px solid #ffffff;
            color: #ffffff;
            text-decoration: none;
            padding: 4px 10px;
            border-radius: 4px;
            margin: 0 10px;
            font-size: 13px;
        }

        .main__our--activities-Swiper .swiper-button-next,
        .main__our--activities-Swiper .swiper-button-prev {
            color: #fff;
        }

        .our_activities--content {
            padding: 20px 30px 10px 15px;
            border-top: 5px solid var(--orangy-yellow);
            background-color: var(--white);
            border-bottom-left-radius: 8px;
            border-bottom-right-radius: 8px;
        }

        .our_activities--content h3 {
            font-weight: 700;
            font-size: 18px;
            color: var(--text-blue);
            overflow: hidden;
            height: 23px;
            line-height: 23px;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 1;
            -webkit-box-orient: vertical;
            text-align: left;
        }

        .our_activities--content h4 {
            text-align: left;
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            color: #000;
            height: 50px;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 2;
            -webkit-box-orient: vertical;
        }

        @media only screen and (max-width: 1313px) {
            .main__our--activities-Swiper {
                max-width: 95% !important;
            }
        }
    </style>

</head>


<div class="main__our--activities">
    <div class="main__container">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2><?php echo get_theme_mod('our_activities_title'); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div #swiperRef="" class="swiper main__our--activities-Swiper">
        <div class="swiper-wrapper">
            <?php
            $our_activities = get_theme_mod('our_activities_count', 3);

            for ($i = 1; $i <= $our_activities; $i++) {
                $activity = get_theme_mod('our_activities_img_' . $i, '');
                if (!empty($activity)) {
            ?>
                    <div class="swiper-slide">
                        <a href="<?php echo get_theme_mod('our_activities_url_' . $i); ?>">
                            <img src="<?php echo get_theme_mod('our_activities_img_' . $i); ?>" alt="Our activities">

                            <div class="our_activities--content">
                                <h3><?php echo get_theme_mod('our_activities_title_' . $i); ?></h3>
                                <h4><?php echo get_theme_mod('our_activities_paragrap_' . $i); ?></h4>
                            </div>
                        </a>
                    </div>
            <?php
                }
            }
            ?>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>



<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

<script>
    var swiper;

    function updateSwiper() {
        if (window.innerWidth < 600) {
            if (swiper && swiper.params.slidesPerView !== 1) {
                swiper.params.slidesPerView = 1;
                swiper.update();
            }
        } else if (window.innerWidth < 996) {
            if (swiper && swiper.params.slidesPerView !== 2) {
                swiper.params.slidesPerView = 2;
                swiper.update();
            }
        } else {
            if (swiper && swiper.params.slidesPerView !== 3) {
                swiper.params.slidesPerView = 3;
                swiper.update();
            }
        }
    }

    swiper = new Swiper(".main__our--activities-Swiper", {
        slidesPerView: 3,
        centeredSlides: false,
        spaceBetween: 65,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        }
    });

    updateSwiper();

    window.addEventListener("resize", updateSwiper);

    var appendNumber = 4;
    var prependNumber = 1;
    document
        .querySelector(".prepend-2-slides")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.prependSlide([
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>",
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>",
            ]);
        });
    document
        .querySelector(".prepend-slide")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.prependSlide(
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>"
            );
        });
    document
        .querySelector(".append-slide")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.appendSlide(
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>"
            );
        });
    document
        .querySelector(".append-2-slides")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.appendSlide([
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>",
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>",
            ]);
        });
</script>