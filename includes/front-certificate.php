<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />

    <style>
        .mySwiper {
            width: 1313px !important;
            padding: 0 65px 50px 65px !important;
        }

        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .swiper-slide img {
            display: block;
            width: 100%;
            height: 100%;
            object-fit: cover;
            padding: 6px 8px;
        }

        .swiper-slide:hover {
            text-decoration: none;
            background-color: rgba(0, 0, 0, 0.04);
            cursor: pointer;
        }

        .swiper {
            width: 100%;
            /* height: 550px; */
        }

        .append-buttons {
            text-align: center;
            margin-top: 20px;
        }

        .append-buttons button {
            display: inline-block;
            cursor: pointer;
            border: 1px solid #007aff;
            color: #007aff;
            text-decoration: none;
            padding: 4px 10px;
            border-radius: 4px;
            margin: 0 10px;
            font-size: 13px;
        }

        @media only screen and (max-width: 1313px) {
            .mySwiper {
                max-width: 95% !important;
            }
        }

        .modal__cer {
            display: none;
            position: fixed;
            z-index: 1000;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgb(0, 0, 0);
            background-color: rgba(0, 0, 0, 0.9);
        }

        .modal__cer-content {
            margin: 10% auto;
            padding: 20px;
            width: 80%;
        }

        .modal__cer img {
            width: 100%;
        }
    </style>
</head>
<div class="main__container">
    <div class="certificate">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>
                        <?php echo get_theme_mod('certificate_title', 'Chứng chỉ'); ?>
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div #swiperRef="" class="swiper mySwiper">
    <div class="swiper-wrapper">
        <?php
        $certificate_count = get_theme_mod('certificate_img_count', 7);

        for ($i = 1; $i <= $certificate_count; $i++) {
            $certificate = get_theme_mod('certificate_img_' . $i, '');
            if (!empty($certificate)) {
                echo '<div class="swiper-slide">';
                echo '<img src="' . $certificate . '" alt="Chứng chỉ">';
                echo '</div>';
            }
        }
        ?>
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
</div>

<div id="myModal__cer" class="modal__cer">
    <div class="modal__cer-content">
        <img id="modalImg" src="">
    </div>
</div>



<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>

<script>
    var swiper;

    function updateSwiper() {
        if (window.innerWidth < 600) {
            if (swiper && swiper.params.slidesPerView !== 1) {
                swiper.params.slidesPerView = 1;
                swiper.update();
            }
        } else if (window.innerWidth < 996) {
            if (swiper && swiper.params.slidesPerView !== 2) {
                swiper.params.slidesPerView = 2;
                swiper.update();
            }
        } else {
            if (swiper && swiper.params.slidesPerView !== 3) {
                swiper.params.slidesPerView = 3;
                swiper.update();
            }
        }
    }

    swiper = new Swiper(".mySwiper", {
        slidesPerView: 3,
        centeredSlides: false,
        spaceBetween: 65,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        on: {
            init: function() {
                attachClickEventsToImages();
            },
        },
    });

    updateSwiper();

    window.addEventListener("resize", updateSwiper);

    var appendNumber = 4;
    var prependNumber = 1;
    document
        .querySelector(".prepend-2-slides")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.prependSlide([
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>",
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>",
            ]);
        });
    document
        .querySelector(".prepend-slide")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.prependSlide(
                '<div class="swiper-slide">Slide ' + --prependNumber + "</div>"
            );
        });
    document
        .querySelector(".append-slide")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.appendSlide(
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>"
            );
        });
    document
        .querySelector(".append-2-slides")
        .addEventListener("click", function(e) {
            e.preventDefault();
            swiper.appendSlide([
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>",
                '<div class="swiper-slide">Slide ' + ++appendNumber + "</div>",
            ]);
        });


    function attachClickEventsToImages() {
        document.querySelectorAll('.swiper-wrapper .swiper-slide img').forEach(item => {
            item.addEventListener('click', function() {
                var modal = document.getElementById('myModal__cer');
                var modalImg = document.getElementById("modalImg");
                modal.style.display = "block";
                modalImg.src = this.src;
            });
        });

        var modal = document.getElementById('myModal__cer');
        modal.addEventListener('click', function() {
            modal.style.display = "none";
        });
    }
</script>