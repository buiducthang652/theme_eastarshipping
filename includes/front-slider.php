<style>
    #slideshow {
        width: 100%;
        height: 680px;
        position: relative;
        overflow: hidden;
    }

    .slide {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-size: cover;
        background-position: center;
        transition: opacity 1s ease-in-out;
    }

    .slide__show {
        opacity: 1 !important;
    }

    .slide__hidden {
        opacity: 0 !important;
    }

    @media only screen and (max-width: 650px) {
        #slideshow {
            height: 250px;
        }
    }
</style>

<div id="slideshow">
    <div class="slide slide__show" style="background-image: url('<?php echo get_theme_mod('slide_img_1'); ?>');"></div>
    <div class="slide slide__hidden" style="background-image: url('<?php echo get_theme_mod('slide_img_2'); ?>');"></div>
    <div class="slide slide__hidden" style="background-image: url('<?php echo get_theme_mod('slide_img_3'); ?>');"></div>
    <div class="slide slide__hidden" style="background-image: url('<?php echo get_theme_mod('slide_img_4'); ?>');"></div>
    <div class="slide slide__hidden" style="background-image: url('<?php echo get_theme_mod('slide_img_5'); ?>');"></div>
    <div class="slide slide__hidden" style="background-image: url('<?php echo get_theme_mod('slide_img_6'); ?>');"></div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        var slides = document.querySelectorAll('.slide');
        var currentSlideIndex = 0;

        function showSlide(index) {
            slides.forEach(function(slide) {
                slide.classList.remove("slide__show");
                slide.classList.add("slide__hidden");
            });
            slides[index].classList.remove("slide__hidden");
            slides[index].classList.add("slide__show");
        }

        function nextSlide() {
            slides[currentSlideIndex].classList.remove("slide__show");
            slides[currentSlideIndex].classList.add("slide__hidden");

            currentSlideIndex = (currentSlideIndex + 1) % slides.length;

            slides[currentSlideIndex].classList.remove("slide__hidden");
            slides[currentSlideIndex].classList.add("slide__show");
        }

        setInterval(nextSlide, 5000);
    });
</script>