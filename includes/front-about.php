<div class="main__about">
    <div class="main__container">
        <div data-aos="fade-down">
            <h2 class="title--one">
                <?php echo get_theme_mod('about_title', "Tiêu đề"); ?>
            </h2>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-4 mt-10">
                    <div data-aos="fade-up" class="about__item">
                        <div class="about__item--top">
                            <div class="about__item--icon">
                                <img src="<?php echo get_theme_file_uri() . '/assets/img/icon-1.png'; ?>" alt="">
                            </div>
                            <div class="about__item--title">
                                TÍNH CHUYÊN NGHIỆP
                            </div>
                        </div>
                        <div class="about__item--content">
                            <?php echo get_theme_mod('professionalism', "Nội dung"); ?>
                            <!-- Chúng tôi đề cao tính chuyên nghiệp của mình trong những gì chúng tôi làm. Chúng tôi có khả năng, hiểu biết và hướng tới tinh thần làm việc theo nhóm trong việc cung cấp các dịch vụ để đáp ứng nhu cầu của khách hàng. -->
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 mt-10">
                    <div data-aos="fade-up" class="about__item">
                        <div class="about__item--top">
                            <div class="about__item--icon">
                                <img src="<?php echo get_theme_file_uri() . '/assets/img/icon-2.png'; ?>" alt="">
                            </div>
                            <div class="about__item--title">
                                LÒNG TIN
                            </div>
                        </div>
                        <div class="about__item--content">
                            <?php echo get_theme_mod('trust', "Nội dung"); ?>
                            <!-- Chúng tôi là những chuyên gia đáng tin cậy. Chúng tôi luôn tôn trọng lẫn nhau, tôn trọng thuyền viên và khách hàng của chúng tôi với trách nhiệm giải trình cá nhân và lẫn nhau. -->
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 mt-10">
                    <div data-aos="fade-up" class="about__item">
                        <div class="about__item--top">
                            <div class="about__item--icon">
                                <img src="<?php echo get_theme_file_uri() . '/assets/img/icon-3.png'; ?>" alt="">
                            </div>
                            <div class="about__item--title">
                                TRÁCH NHIỆM
                            </div>
                        </div>
                        <div class="about__item--content">
                            <?php echo get_theme_mod('responsibility', "Nội dung"); ?>
                            <!-- Chúng tôi quan tâm đúng mức và có trách nhiệm trong việc thực hiện các cam kết của mình với thuyền viên, khách hàng và xã hội. -->
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 mt-10">
                    <div data-aos="fade-up" class="about__item">
                        <div class="about__item--top">
                            <div class="about__item--icon">
                                <img src="<?php echo get_theme_file_uri() . '/assets/img/icon-4.png'; ?>" alt="">
                            </div>
                            <div class="about__item--title">
                                CHU ĐÁO & TẬN TÂM
                            </div>
                        </div>
                        <div class="about__item--content">
                            <?php echo get_theme_mod('attentive_dedicated', "Nội dung"); ?>
                            <!-- Chúng tôi luôn luôn chu đáo và tận tâm trong việc đảm nhận công việc và trách nhiệm của mình. -->
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 mt-10">
                    <div data-aos="fade-up" class="about__item">
                        <div class="about__item--top">
                            <div class="about__item--icon">
                                <img src="<?php echo get_theme_file_uri() . '/assets/img/icon-5.png'; ?>" alt="">
                            </div>
                            <div class="about__item--title">
                                SỰ TRUNG THÀNH
                            </div>
                        </div>
                        <div class="about__item--content">
                            <?php echo get_theme_mod('loyalty', "Nội dung"); ?>
                            <!-- Chúng tôi là những chuyên gia trung thành và trung thực. Chúng tôi luôn cố gắng hết sức để duy trì tính chính trực và tuân thủ vững chắc quy tắc đạo đức của mình. Chúng tôi tự hào được làm việc với các khách hàng của mình. -->
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 mt-10">
                    <div data-aos="fade-up" class="about__item">
                        <div class="about__item--top">
                            <div class="about__item--icon">
                                <img src="<?php echo get_theme_file_uri() . '/assets/img/icon-6.png'; ?>" alt="">
                            </div>
                            <div class="about__item--title">
                                TÍNH MINH BẠCH & ĐẠO ĐỨC
                            </div>
                        </div>
                        <div class="about__item--content">
                            <?php echo get_theme_mod('transparency_ethics', "Nội dung"); ?>
                            <!-- Chúng tôi đảm bảo tính minh bạch trong tất cả các chính sách và thông lệ của mình, đồng thời nhất quán và công bằng trong cách chúng tôi áp dụng các chính sách và giá trị của mình. Chúng tôi chấp nhận trách nhiệm cộng đồng của mình và giao tiếp trung thực với các bên liên quan của chúng tôi. -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div data-aos="fade-up">
            <div class="text-center">
                <h2 class="title--one title--two">
                    <?php echo get_theme_mod('about_title_introduce', "Lời giới thiệu"); ?>
                </h2>
            </div>

            <div class="container mb-36">
                <div class="row">
                    <div class="col-12 col-md-5 d-flex align-items-center">
                        <div class="about__bot--vid">
                            <img class="vid--title" src="<?php echo get_theme_mod('youtube_about_image') ?>" alt="image video">
                            <img class="vid--btn" data-bs-toggle="modal" data-bs-target="#exampleModal" src="<?php echo get_template_directory_uri() . '/assets/img/PlayButton.svg'; ?>" alt="btn-video">
                        </div>
                    </div>
                    <div class="col-12 col-md-7 about__bot--content">
                        <?php
                        $paragraph_count = get_theme_mod('about_paragraph_count', 3);

                        for ($i = 1; $i <= $paragraph_count; $i++) {
                            $paragraph = get_theme_mod('about_paragraph_' . $i, '');
                            if (!empty($paragraph)) {
                                echo '<p>' . esc_html($paragraph) . '</p>';
                            }
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content modal__dialog--vid" id="id_video_modal">
            <?php
            $video_url = get_theme_mod('youtube_video_url', '');

            if (!empty($video_url)) {
                echo wp_oembed_get($video_url);
            }
            ?>
        </div>
    </div>
</div>