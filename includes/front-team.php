<div class="main__container">
    <div class="team">
        <div class="container" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="700">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>
                        <?php echo get_theme_mod('team_title', 'Đội ngũ của chúng tôi') ?>
                    </h2>
                </div>
                <div class="col-12 text-center">
                    <p>
                        <?php echo get_theme_mod('team_des', 'Giới thiệu') ?>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-12" data-aos="fade-right">
                    <div class="team__member1">
                        <div class="member1--img">
                            <img src="<?php echo get_theme_mod('team_member_img_1'); ?>" alt="image member 1">
                        </div>
                        <div class="member1--info">
                            <div class="member1--info-main">
                                <h5><?php echo get_theme_mod('team_member_name_1'); ?></h5>
                                <p><?php echo get_theme_mod('team_member_position_1'); ?></p>
                            </div>
                            <p class="member1--info-caption">
                                <?php echo get_theme_mod('team_member_caption_1'); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12" data-aos="fade-left">
                    <div class="team__member2">
                        <div class="member2--info">
                            <div class="member2--info-main">
                                <h5><?php echo get_theme_mod('team_member_name_2'); ?></h5>
                                <p><?php echo get_theme_mod('team_member_position_2'); ?></p>
                            </div>
                            <p class="member2--info-caption">
                                <?php echo get_theme_mod('team_member_caption_2'); ?>
                            </p>
                        </div>
                        <div class="member2--img">
                            <img src="<?php echo get_theme_mod('team_member_img_2'); ?>" alt="image member 1">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-xl-4" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                    <div class="our_team__member1">
                        <div class="our_team__member1--img">
                            <img src="<?php echo get_theme_mod('team_member_img_3'); ?>" alt="">
                        </div>
                        <div class="our_team__member1--info">
                            <div class="our_team__member1--title">
                                <h5>
                                    <?php echo get_theme_mod('team_member_name_3'); ?>
                                </h5>
                                <p>
                                    <?php echo get_theme_mod('team_member_position_3'); ?>
                                </p>
                            </div>
                            <p class="our_team__member1--des">
                                <?php echo get_theme_mod('team_member_caption_3'); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-4" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="600" data-aos-offset="0">
                    <div class="our_team__member1">
                        <div class="our_team__member1--img">
                            <img src="<?php echo get_theme_mod('team_member_img_4'); ?>" alt="">
                        </div>
                        <div class="our_team__member1--info">
                            <div class="our_team__member1--title">
                                <h5>
                                    <?php echo get_theme_mod('team_member_name_4'); ?>
                                </h5>
                                <p>
                                    <?php echo get_theme_mod('team_member_position_4'); ?>
                                </p>
                            </div>
                            <p class="our_team__member1--des">
                                <?php echo get_theme_mod('team_member_caption_4'); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-4" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="900" data-aos-offset="0">
                    <div class="our_team__member1">
                        <div class="our_team__member1--img">
                            <img src="<?php echo get_theme_mod('team_member_img_5'); ?>" alt="">
                        </div>
                        <div class="our_team__member1--info">
                            <div class="our_team__member1--title">
                                <h5>
                                    <?php echo get_theme_mod('team_member_name_5'); ?>
                                </h5>
                                <p>
                                    <?php echo get_theme_mod('team_member_position_5'); ?>
                                </p>
                            </div>
                            <p class="our_team__member1--des">
                                <?php echo get_theme_mod('team_member_caption_5'); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-xl-3 mt-3 mt-md-0" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                    <div class="our_team__member2">
                        <div class="our_team__member2--title">
                            <h5>
                                <?php echo get_theme_mod('team_member_name_6'); ?>
                            </h5>
                        </div>
                        <p class="our_team__member2--des">
                            <?php echo get_theme_mod('team_member_caption_6'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-3 mt-3 mt-md-0" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="600" data-aos-offset="0">
                    <div class="our_team__member2">
                        <div class="our_team__member2--title">
                            <h5>
                                <?php echo get_theme_mod('team_member_name_7'); ?>
                            </h5>
                        </div>
                        <p class="our_team__member2--des">
                            <?php echo get_theme_mod('team_member_caption_7'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-3 mt-3 mt-xl-0" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="900" data-aos-offset="0">
                    <div class="our_team__member2">
                        <div class="our_team__member2--title">
                            <h5>
                                <?php echo get_theme_mod('team_member_name_8'); ?>
                            </h5>
                        </div>
                        <p class="our_team__member2--des">
                            <?php echo get_theme_mod('team_member_caption_8'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-3 mt-3 mt-xl-0" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="1200" data-aos-offset="0">
                    <div class="our_team__member2">
                        <div class="our_team__member2--title">
                            <h5>
                                <?php echo get_theme_mod('team_member_name_9'); ?>
                            </h5>
                        </div>
                        <p class="our_team__member2--des">
                            <?php echo get_theme_mod('team_member_caption_9'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>