<div class="main__vision--misson">
    <div class="main__container">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center flex-column">
                    <div data-aos="fade-right" class="vision__misson">
                        <img src="<?php echo get_template_directory_uri() . '/assets/img/Visibility.svg'; ?>" alt="">
                        <h3 class="vision--title">
                            <?php echo get_theme_mod('colu_one_title', 'Tầm nhìn') ?>
                        </h3>
                        <div class="content--devece"></div>
                        <div class="vision--content">
                            <?php echo get_theme_mod('colu_one_content', 'Nội dung') ?>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 col-lg-6 d-flex justify-content-center align-items-center flex-column">
                    <div data-aos="fade-left" class="vision__misson">
                        <img src="<?php echo get_template_directory_uri() . '/assets/img/Anchor.svg'; ?>" alt="">
                        <h3 class="misson--title">
                            <?php echo get_theme_mod('colu_two_title', 'Sứ mệnh') ?>
                        </h3>
                        <div class="content--devece"></div>
                        <div class="misson--content">
                            <?php echo get_theme_mod('colu_two_content', 'Nội dung') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>