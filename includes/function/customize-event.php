<?php

function event_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_event',
        array(
            'title' => 'Blog: Tin tức sự kiện',
        )
    );

    $wp_customize->add_setting(
        'event_breadcrumb',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'event_breadcrumb',
        [
            'label' => 'Breadcrumb',
            'section' => 'sec_event',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'news_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'news_title',
        [
            'label' => 'Tiêu đề (News)',
            'section' => 'sec_event',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'event_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'event_title',
        [
            'label' => 'Tiêu đề (Event)',
            'section' => 'sec_event',
            'type' => 'text',
        ]
    );

    // Hình ảnh
    $wp_customize->add_setting(
        'event_img_count',
        [
            'default' => 3,
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'event_img_count',
        [
            'label' => 'Chọn số chứng chỉ hiển thị (Event)',
            'description' => 'Trong khoảng 1 - 25',
            'section' => 'sec_event',
            'type' => 'number',
            'input_attrs' => array(
                'min' => 1,
                'max' => 25,
                'step' => 1,
            ),
        ]
    );

    for ($i = 1; $i <= 25; $i++) {
        $wp_customize->add_setting(
            'event_img_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'event_img_' . $i,
                array(
                    'label' => 'Hình ảnh hiển thị ' . $i,
                    'section' => 'sec_event',
                    'settings' => 'event_img_' . $i,
                    'active_callback' => function ($control) use ($i) {
                        $count = $control->manager->get_setting('event_img_count')->value();
                        return $i <= $count;
                    },
                )
            )
        );
    }
}

add_action('customize_register', 'event_customize');
