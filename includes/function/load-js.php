<?php

// Load file JS
function load_custom_js()
{
    // Nhúng JavaScript của Bootstrap 
    wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js', array('jquery'), '5.3.2', true);

    // JS chung cho website
    wp_enqueue_script('custom-main-js', get_stylesheet_directory_uri() . '/assets/js/main.js', array('jquery'), '1.0', true);

}

add_action('wp_enqueue_scripts', 'load_custom_js');
