<?php

function certificate_our_team_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_certificate_our_team',
        array(
            'title' => 'Main: Certificate our team',
        )
    );

    // Tiêu đề chứng chỉ
    $wp_customize->add_setting(
        'certificate_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'certificate_title',
        [
            'label' => 'Tiêu đề (Certificate)',
            'section' => 'sec_certificate_our_team',
            'type' => 'text',
        ]
    );

    // Hình ảnh
    $wp_customize->add_setting(
        'certificate_img_count',
        [
            'default' => 3,
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'certificate_img_count',
        [
            'label' => 'Chọn số chứng chỉ hiển thị (Certificate)',
            'description' => 'Trong khoảng 1 - 10',
            'section' => 'sec_certificate_our_team',
            'type' => 'number',
            'input_attrs' => array(
                'min' => 1,
                'max' => 10,
                'step' => 1,
            ),
        ]
    );

    for ($i = 1; $i <= 10; $i++) {
        $wp_customize->add_setting(
            'certificate_img_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'certificate_img_' . $i,
                array(
                    'label' => 'Hình ảnh hiển thị ' . $i,
                    'section' => 'sec_certificate_our_team',
                    'settings' => 'certificate_img_' . $i,
                    'active_callback' => function ($control) use ($i) {
                        $count = $control->manager->get_setting('certificate_img_count')->value();
                        return $i <= $count;
                    },
                )
            )
        );
    }

    // Tiêu đề thành viên
    $wp_customize->add_setting(
        'team_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'team_title',
        [
            'label' => 'Tiêu đề (Team)',
            'section' => 'sec_certificate_our_team',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'team_des',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'team_des',
        [
            'label' => 'Mô tả (Team)',
            'section' => 'sec_certificate_our_team',
            'type' => 'text',
        ]
    );

    // Thành viên 1
    for ($i = 1; $i <= 5; $i++) {

        $wp_customize->add_setting(
            'team_member_name_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'team_member_name_' . $i,
            [
                'label' => 'Tên thành viên ' . $i . ' (Team)',
                'section' => 'sec_certificate_our_team',
                'type' => 'text',
            ]
        );

        $wp_customize->add_setting(
            'team_member_position_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'team_member_position_' . $i,
            [
                'label' => 'Chức vụ thành viên ' . $i . ' (Team)',
                'section' => 'sec_certificate_our_team',
                'type' => 'text',
            ]
        );

        $wp_customize->add_setting(
            'team_member_caption_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'team_member_caption_' . $i,
            [
                'label' => 'Giới thiệu thành viên ' . $i . ' (Team)',
                'section' => 'sec_certificate_our_team',
                'type' => 'textarea',
            ]
        );

        $wp_customize->add_setting(
            'team_member_img_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'team_member_img_' . $i,
                array(
                    'label' => 'Hình ảnh hiển thị ' . $i,
                    'section' => 'sec_certificate_our_team',
                    'settings' => 'team_member_img_' . $i,
                )
            )
        );
    }

    for ($i = 6; $i <= 9; $i++) {
        $wp_customize->add_setting(
            'team_member_name_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'team_member_name_' . $i,
            [
                'label' => 'Tên thành viên ' . $i . ' (Team)',
                'section' => 'sec_certificate_our_team',
                'type' => 'text',
            ]
        );

        $wp_customize->add_setting(
            'team_member_caption_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'team_member_caption_' . $i,
            [
                'label' => 'Giới thiệu thành viên ' . $i . ' (Team)',
                'section' => 'sec_certificate_our_team',
                'type' => 'textarea',
            ]
        );
    }

    for ($i = 1; $i <= 4; $i++) {
        $wp_customize->add_setting(
            'infomation_title_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'infomation_title_' . $i,
            [
                'label' => 'Số lượng (Infomation) ' . $i,
                'description' => 'Nhập vào một số...',
                'section' => 'sec_certificate_our_team',
                'type' => 'number',
            ]
        );

        $wp_customize->add_setting(
            'infomation_sign_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'infomation_sign_' . $i,
            [
                'label' => 'Kí hiệu sau (Infomation) ' . $i,
                'section' => 'sec_certificate_our_team',
                'type' => 'text',
            ]
        );

        $wp_customize->add_setting(
            'infomation_name_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'infomation_name_' . $i,
            [
                'label' => 'Tên thông tin (Infomation) ' . $i,
                'section' => 'sec_certificate_our_team',
                'type' => 'text',
            ]
        );
    }
}

add_action('customize_register', 'certificate_our_team_customize');
