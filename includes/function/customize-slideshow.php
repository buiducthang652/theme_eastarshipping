<?php

function slideshow_customize($wp_customize)
{

    $wp_customize->add_section(
        'sec_slideshow',
        [
            'title' => 'Main: Slide show',
        ]
    );

    $wp_customize->add_setting('slide_img_1');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'slide_img_1',
            array(
                'label' => 'Hình ảnh slide số 1',
                'section' => 'sec_slideshow',
                'settings' => 'slide_img_1'
            )
        )
    );

    $wp_customize->add_setting('slide_img_2');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'slide_img_2',
            array(
                'label' => 'Hình ảnh slide số 2',
                'section' => 'sec_slideshow',
                'settings' => 'slide_img_2'
            )
        )
    );

    $wp_customize->add_setting('slide_img_3');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'slide_img_3',
            array(
                'label' => 'Hình ảnh slide số 3',
                'section' => 'sec_slideshow',
                'settings' => 'slide_img_3'
            )
        )
    );

    $wp_customize->add_setting('slide_img_4');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'slide_img_4',
            array(
                'label' => 'Hình ảnh slide số 4',
                'section' => 'sec_slideshow',
                'settings' => 'slide_img_4'
            )
        )
    );

    $wp_customize->add_setting('slide_img_5');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'slide_img_5',
            array(
                'label' => 'Hình ảnh slide số 5',
                'section' => 'sec_slideshow',
                'settings' => 'slide_img_5'
            )
        )
    );

    $wp_customize->add_setting('slide_img_6');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'slide_img_6',
            array(
                'label' => 'Hình ảnh slide số 6',
                'section' => 'sec_slideshow',
                'settings' => 'slide_img_6'
            )
        )
    );
}

add_action('customize_register', 'slideshow_customize');
