<?php
// Load file CSS
function load_custom_css()
{
    // Nhúng CSS của Bootstrap
    wp_enqueue_style('bootstrap-css', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css', array(), '5.3.2');

    // Font chữ tùy chỉnh (Đảm bảo đường dẫn đúng và không có ký tự escape)
    wp_enqueue_style('roboto-font', 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap', false);

    // CSS chung cho website
    wp_enqueue_style('custom-style', get_stylesheet_directory_uri() . '/assets/css/style.css', array(), '1.0');
}

add_action('wp_enqueue_scripts', 'load_custom_css');
