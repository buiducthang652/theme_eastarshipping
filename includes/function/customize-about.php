<?php

function about_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_about',
        array(
            'title' => 'Main: About',
        )
    );

    $wp_customize->add_setting(
        'about_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'about_title',
        [
            'label' => 'Tiêu đề',
            'section' => 'sec_about',
            'type' => 'text',
        ]
    );

    // Tính chuyên nghiệp
    $wp_customize->add_setting(
        'professionalism',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'professionalism',
        [
            'label' => 'Nội dung tính chuyên nghiệp',
            'section' => 'sec_about',
            'type' => 'text',
        ]
    );

    // Lòng tin
    $wp_customize->add_setting(
        'trust',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'trust',
        [
            'label' => 'Nội dung lòng tin',
            'section' => 'sec_about',
            'type' => 'text',
        ]
    );

    // Trách nhiệm
    $wp_customize->add_setting(
        'responsibility',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'responsibility',
        [
            'label' => 'Nội dung trách nhiệm',
            'section' => 'sec_about',
            'type' => 'text',
        ]
    );

    // Chu đáo và tận tâm
    $wp_customize->add_setting(
        'attentive_dedicated',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'attentive_dedicated',
        [
            'label' => 'Nội dung chu đáo & tận tâm',
            'section' => 'sec_about',
            'type' => 'text',
        ]
    );

    // Sự trung thành
    $wp_customize->add_setting(
        'loyalty',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'loyalty',
        [
            'label' => 'Nội dung sự trung thành',
            'section' => 'sec_about',
            'type' => 'text',
        ]
    );

    // Minh bạch và đạo đức
    $wp_customize->add_setting(
        'transparency_ethics',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'transparency_ethics',
        [
            'label' => 'Nội dung tính minh bạch & đạo đức',
            'section' => 'sec_about',
            'type' => 'text',
        ]
    );

    // Giới thiệu
    $wp_customize->add_setting(
        'about_title_introduce',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'about_title_introduce',
        [
            'label' => 'Tiêu đề giới thiệu',
            'section' => 'sec_about',
            'type' => 'text',
        ]
    );

    // Hình ảnh
    $wp_customize->add_setting('youtube_about_image');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'youtube_about_image',
            array(
                'label' => 'Hình ảnh hiển thị',
                'section' => 'sec_about',
                'settings' => 'youtube_about_image'
            )
        )
    );

    $wp_customize->add_setting(
        'youtube_video_url',
        array(
            'default' => '',
            'sanitize_callback' => 'esc_url_raw',
        )
    );

    $wp_customize->add_control(
        'youtube_video_url',
        array(
            'label' => 'Đường dẫn video youtube',
            'section' => 'sec_about',
            'type' => 'text',
        )
    );


    // Nội dung
    $wp_customize->add_setting(
        'about_paragraph_count',
        [
            'default' => 3,
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'about_paragraph_count',
        [
            'label' => 'Số lượng đoạn văn bản',
            'description' => 'Trong khoảng 1 - 10',
            'section' => 'sec_about',
            'type' => 'number',
            'input_attrs' => array(
                'min' => 1,
                'max' => 10,
                'step' => 1,
            ),
        ]
    );

    for ($i = 1; $i <= 10; $i++) {
        $wp_customize->add_setting(
            'about_paragraph_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'about_paragraph_' . $i,
            [
                'label' => 'Đoạn văn bản ' . $i,
                'section' => 'sec_about',
                'type' => 'textarea',
                'active_callback' => function ($control) use ($i) {
                    $count = $control->manager->get_setting('about_paragraph_count')->value();
                    return $i <= $count;
                },
            ]
        );
    }
}

add_action('customize_register', 'about_customize');
