<?php

function contact_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_contact',
        array(
            'title' => 'Contact',
        )
    );

    $wp_customize->add_setting(
        'contact_breadcrumb',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'contact_breadcrumb',
        [
            'label' => 'Breadcrumb',
            'section' => 'sec_contact',
            'type' => 'text',
        ]
    );
    $wp_customize->add_setting(
        'contact_address',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'contact_address',
        [
            'label' => 'Địa chỉ',
            'section' => 'sec_contact',
            'type' => 'text',
        ]
    );
    $wp_customize->add_setting(
        'contact_phone',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'contact_phone',
        [
            'label' => 'Số điện thoại',
            'section' => 'sec_contact',
            'type' => 'text',
        ]
    );
    $wp_customize->add_setting(
        'contact_email',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'contact_email',
        [
            'label' => 'Email',
            'section' => 'sec_contact',
            'type' => 'text',
        ]
    );
}

add_action('customize_register', 'contact_customize');
