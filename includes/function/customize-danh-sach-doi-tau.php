<?php

function fleet_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_fleet',
        array(
            'title' => 'Fleet',
        )
    );

    $wp_customize->add_setting(
        'fleet_breadcrumb',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'fleet_breadcrumb',
        [
            'label' => 'Breadcrumb',
            'section' => 'sec_fleet',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'table_fleet_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'table_fleet_title',
        [
            'label' => 'Tiêu đề bảng',
            'section' => 'sec_fleet',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'table_fleet_des',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'table_fleet_des',
        [
            'label' => 'Mô tả bảng',
            'section' => 'sec_fleet',
            'type' => 'text',
        ]
    );
}

add_action('customize_register', 'fleet_customize');
