<?php

function header_customize($wp_customize)
{

    $wp_customize->add_section(
        'sec_socialmedia',
        [
            'title' => 'Header: Social Media',
        ]
    );

    $wp_customize->add_setting(
        'linkedin_link',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'linkedin_link',
        [
            'label' => 'Linkedin Link',
            'section' => 'sec_socialmedia',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'facebook_link',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'facebook_link',
        [
            'label' => 'Facebook Link',
            'section' => 'sec_socialmedia',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'youtube_link',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'youtube_link',
        [
            'label' => 'Youtube Link',
            'section' => 'sec_socialmedia',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'twitter_link',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'twitter_link',
        [
            'label' => 'Twitter Link',
            'section' => 'sec_socialmedia',
            'type' => 'text',
        ]
    );
    
}

add_action('customize_register', 'header_customize');

