<?php

function service_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_service',
        array(
            'title' => 'Service',
        )
    );

    $wp_customize->add_setting(
        'service_breadcrumb',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'service_breadcrumb',
        [
            'label' => 'Breadcrumb',
            'section' => 'sec_service',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'service_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'service_title',
        [
            'label' => 'Tiêu đề',
            'section' => 'sec_service',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'service_title_1',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'service_title_1',
        [
            'label' => 'Tiêu đề 1',
            'section' => 'sec_service',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'service_content_1',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'service_content_1',
        [
            'label' => 'Nội dung 1',
            'section' => 'sec_service',
            'type' => 'text',
        ]
    );

    // Hình ảnh
    $wp_customize->add_setting('service_img_1');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'service_img_1',
            array(
                'label' => 'Hình ảnh hiển thị 1',
                'section' => 'sec_service',
                'settings' => 'service_img_1'
            )
        )
    );

    $wp_customize->add_setting(
        'service_title_2',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'service_title_2',
        [
            'label' => 'Tiêu đề 2',
            'section' => 'sec_service',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'service_content_2',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'service_content_2',
        [
            'label' => 'Nội dung 2',
            'section' => 'sec_service',
            'type' => 'text',
        ]
    );

    // Hình ảnh
    $wp_customize->add_setting('service_img_21');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'service_img_21',
            array(
                'label' => 'Hình ảnh hiển thị 2.1',
                'section' => 'sec_service',
                'settings' => 'service_img_21'
            )
        )
    );

    $wp_customize->add_setting('service_img_22');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'service_img_22',
            array(
                'label' => 'Hình ảnh hiển thị 2.2',
                'section' => 'sec_service',
                'settings' => 'service_img_22'
            )
        )
    );

    $wp_customize->add_setting(
        'service_title_3',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'service_title_3',
        [
            'label' => 'Tiêu đề 3',
            'section' => 'sec_service',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'service_content_3',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'service_content_3',
        [
            'label' => 'Nội dung 3',
            'section' => 'sec_service',
            'type' => 'text',
        ]
    );

    // Hình ảnh
    $wp_customize->add_setting('service_img_31');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'service_img_31',
            array(
                'label' => 'Hình ảnh hiển thị 3.1',
                'section' => 'sec_service',
                'settings' => 'service_img_31'
            )
        )
    );

    $wp_customize->add_setting('service_img_32');
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'service_img_32',
            array(
                'label' => 'Hình ảnh hiển thị 3.2',
                'section' => 'sec_service',
                'settings' => 'service_img_32'
            )
        )
    );
}

add_action('customize_register', 'service_customize');
