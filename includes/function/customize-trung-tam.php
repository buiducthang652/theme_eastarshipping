<?php

function training_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_training',
        array(
            'title' => 'Training',
        )
    );

    $wp_customize->add_setting(
        'training_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'training_title',
        [
            'label' => 'Tiêu đề trang training',
            'section' => 'sec_training',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'training_breadcrumb',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'training_breadcrumb',
        [
            'label' => 'Breadcrumb',
            'section' => 'sec_training',
            'type' => 'text',
        ]
    );

    // Nội dung
    $wp_customize->add_setting(
        'training_paragraph_count',
        [
            'default' => 2,
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'training_paragraph_count',
        [
            'label' => 'Số lượng đoạn văn bản',
            'description' => 'Trong khoảng 1 - 10',
            'section' => 'sec_training',
            'type' => 'number',
            'input_attrs' => array(
                'min' => 1,
                'max' => 10,
                'step' => 1,
            ),
        ]
    );

    for ($i = 1; $i <= 10; $i++) {
        $wp_customize->add_setting(
            'training_paragraph_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'training_paragraph_' . $i,
            [
                'label' => 'Đoạn văn bản ' . $i,
                'section' => 'sec_training',
                'type' => 'textarea',
                'active_callback' => function ($control) use ($i) {
                    $count = $control->manager->get_setting('training_paragraph_count')->value();
                    return $i <= $count;
                },
            ]
        );
    }

    for ($j = 1; $j <= 3; $j++) {
        // swiper
        $wp_customize->add_setting(
            'training_title_' . $j,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'training_title_' . $j,
            [
                'label' => 'Tiêu đề training ' . $j,
                'section' => 'sec_training',
                'type' => 'text',
            ]
        );

        $wp_customize->add_setting(
            'training_swiper' . $j . '_img',
            [
                'default' => 5,
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'training_swiper' . $j . '_img',
            [
                'label' => 'Số lượng hình ảnh',
                'description' => 'Trong khoảng 1 - 25',
                'section' => 'sec_training',
                'type' => 'number',
                'input_attrs' => array(
                    'min' => 1,
                    'max' => 25,
                    'step' => 1,
                ),
            ]
        );

        for ($i = 1; $i <= 25; $i++) {
            $wp_customize->add_setting(
                'training_swiper' . $j . '_img_' . $i,
                [
                    'default' => '',
                    'transport' => 'refresh',
                ]
            );

            $wp_customize->add_control(
                new WP_Customize_Image_Control(
                    $wp_customize,
                    'training_swiper' . $j . '_img_' . $i,
                    array(
                        'label' => 'Hình ảnh hiển thị ' . $i,
                        'section' => 'sec_training',
                        'settings' => 'training_swiper' . $j . '_img_' . $i,
                        'active_callback' => function () use ($j, $i) { // Sửa ở đây
                            $count = get_theme_mod('training_swiper' . $j . '_img', 5); // Sửa lại cách lấy giá trị
                            return $i <= $count;
                        },
                    )
                )
            );
        }
    }
}

add_action('customize_register', 'training_customize');
