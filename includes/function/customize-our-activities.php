<?php

function our_activities_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_our_activities',
        array(
            'title' => 'Main: Our Activities',
        )
    );

    $wp_customize->add_setting(
        'our_activities_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'our_activities_title',
        [
            'label' => 'Tiêu đề',
            'section' => 'sec_our_activities',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'our_activities_count',
        [
            'default' => 3,
            'transport' => 'refresh',
            'sanitize_callback' => 'wp_filter_nohtml_kses',
        ]
    );


    $wp_customize->add_control(
        'our_activities_count',
        [
            'label' => 'Chọn số lượng hiển thị',
            'description' => 'Trong khoảng 1 - 10',
            'section' => 'sec_our_activities',
            'type' => 'number',
            'input_attrs' => array(
                'min' => 1,
                'max' => 10,
                'step' => 1,
            ),
        ]
    );

    for ($i = 1; $i <= 10; $i++) {
        $wp_customize->add_setting(
            'our_activities_img_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'our_activities_img_' . $i,
                array(
                    'label' => 'Hình ảnh hiển thị ' . $i,
                    'section' => 'sec_our_activities',
                    'settings' => 'our_activities_img_' . $i,
                    'active_callback' => function ($control) use ($i) {
                        $count = $control->manager->get_setting('our_activities_count')->value();
                        return $i <= $count;
                    },
                )
            )
        );

        $wp_customize->add_setting(
            'our_activities_title_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'our_activities_title_' . $i,
            [
                'label' => 'Tiêu đề ' . $i,
                'section' => 'sec_our_activities',
                'type' => 'text',
                'active_callback' => function ($control) use ($i) {
                    $count = $control->manager->get_setting('our_activities_count')->value();
                    return $i <= $count;
                },
            ]
        );

        $wp_customize->add_setting(
            'our_activities_paragrap_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'our_activities_paragrap_' . $i,
            [
                'label' => 'Mô tả ' . $i,
                'section' => 'sec_our_activities',
                'type' => 'textarea',
                'active_callback' => function ($control) use ($i) {
                    $count = $control->manager->get_setting('our_activities_count')->value();
                    return $i <= $count;
                },
            ]
        );

        $wp_customize->add_setting(
            'our_activities_url_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'our_activities_url_' . $i,
            [
                'label' => 'Đường dẫn ' . $i,
                'section' => 'sec_our_activities',
                'type' => 'text',
                'active_callback' => function ($control) use ($i) {
                    $count = $control->manager->get_setting('our_activities_count')->value();
                    return $i <= $count;
                },
            ]
        );
    }
}

add_action('customize_register', 'our_activities_customize');
