<?php

function footer_customize($wp_customize)
{

    $wp_customize->add_section(
        'sec_footer',
        [
            'title' => 'Footer',
        ]
    );

    $wp_customize->add_setting(
        'address_office',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'address_office',
        [
            'label' => 'Địa chỉ văn phòng: ',
            'section' => 'sec_footer',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'address_office_link',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'address_office_link',
        [
            'label' => 'Google map văn phòng: ',
            'section' => 'sec_footer',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'phone',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'phone',
        [
            'label' => 'Điện thoại: ',
            'section' => 'sec_footer',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'hotline',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'hotline',
        [
            'label' => 'Đường dây nóng: ',
            'section' => 'sec_footer',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'email',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'email',
        [
            'label' => 'E-mail: ',
            'section' => 'sec_footer',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'student_address',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'student_address',
        [
            'label' => 'Chỗ ở học viên: ',
            'section' => 'sec_footer',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'business_address',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'business_address',
        [
            'label' => 'Địa chỉ đăng kí kinh doanh: ',
            'section' => 'sec_footer',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'copyrights',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'copyrights',
        [
            'label' => 'Bản quyền (copyright): ',
            'section' => 'sec_footer',
            'type' => 'text',
        ]
    );
}

add_action('customize_register', 'footer_customize');
