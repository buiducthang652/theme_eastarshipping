<?php

function advance_client_customize($wp_customize)
{
    $wp_customize->add_section(
        'sec_advance_client',
        array(
            'title' => 'Main: Advance Client',
        )
    );

    $wp_customize->add_setting(
        'advance_client_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'advance_client_title',
        [
            'label' => 'Tiêu đề',
            'section' => 'sec_advance_client',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'advance_client_count',
        [
            'default' => 3,
            'transport' => 'refresh',
            'sanitize_callback' => 'wp_filter_nohtml_kses',
        ]
    );


    $wp_customize->add_control(
        'advance_client_count',
        [
            'label' => 'Chọn số lượng hiển thị',
            'description' => 'Trong khoảng 1 - 10',
            'section' => 'sec_advance_client',
            'type' => 'number',
            'input_attrs' => array(
                'min' => 1,
                'max' => 10,
                'step' => 1,
            ),
        ]
    );

    for ($i = 1; $i <= 10; $i++) {
        $wp_customize->add_setting(
            'advance_client_img_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'advance_client_img_' . $i,
                array(
                    'label' => 'Hình ảnh hiển thị ' . $i,
                    'section' => 'sec_advance_client',
                    'settings' => 'advance_client_img_' . $i,
                    'active_callback' => function ($control) use ($i) {
                        $count = $control->manager->get_setting('advance_client_count')->value();
                        return $i <= $count;
                    },
                )
            )
        );

        $wp_customize->add_setting(
            'advance_client_url_' . $i,
            [
                'default' => '',
                'transport' => 'refresh',
            ]
        );

        $wp_customize->add_control(
            'advance_client_url_' . $i,
            [
                'label' => 'Đường dẫn ' . $i,
                'section' => 'sec_advance_client',
                'type' => 'text',
                'active_callback' => function ($control) use ($i) {
                    $count = $control->manager->get_setting('advance_client_count')->value();
                    return $i <= $count;
                },
            ]
        );
    }
}

add_action('customize_register', 'advance_client_customize');
