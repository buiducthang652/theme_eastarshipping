<?php

function mision_misson_customize($wp_customize)
{

    $wp_customize->add_section(
        'sec_vision_misson',
        [
            'title' => 'Main: Vision misson',
        ]
    );

    // Cột 1
    $wp_customize->add_setting(
        'colu_one_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'colu_one_title',
        [
            'label' => 'Tiêu đề cột 1: ',
            'section' => 'sec_vision_misson',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'colu_one_content',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'colu_one_content',
        [
            'label' => 'Nội dung cột 1: ',
            'section' => 'sec_vision_misson',
            'type' => 'text',
        ]
    );

    // Cột 2
    $wp_customize->add_setting(
        'colu_two_title',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'colu_two_title',
        [
            'label' => 'Tiêu đề cột 2: ',
            'section' => 'sec_vision_misson',
            'type' => 'text',
        ]
    );

    $wp_customize->add_setting(
        'colu_two_content',
        [
            'default' => '',
            'transport' => 'refresh',
        ]
    );

    $wp_customize->add_control(
        'colu_two_content',
        [
            'label' => 'Nội dung cột 2: ',
            'section' => 'sec_vision_misson',
            'type' => 'text',
        ]
    );
}

add_action('customize_register', 'mision_misson_customize');
