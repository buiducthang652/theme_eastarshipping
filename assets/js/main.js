jQuery(document).ready(function ($) {
  $(".eastar__header--icon-Menu>span").click(function (event) {
    $(".eastar__header .container-fluid").addClass("addClassRight");
    $(".header__ball").css("display", "block");
    $("body").addClass("ovfl-hi");
  });

  $(".header__ball").click(function (event) {
    hideContainerFluid();
  });

  $(window).resize(function () {
    hideContainerFluid();
  });

  function hideContainerFluid() {
    $(".eastar__header .container-fluid").removeClass("addClassRight");
    $(".header__ball").css("display", "none");
    $("body").removeClass("ovfl-hi");
  }
});
